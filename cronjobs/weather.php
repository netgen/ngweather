<?php
$cli = eZCLI::instance();
$db = eZDB::instance();

$sys = eZSys::instance();

$forecast=false;

$location_codes = eZINI::instance('ngweather.ini')->variable('Locations', 'LocationCode');

$forecast = Weather::getForecast($location_codes, 'metric');
if ($forecast)
{
	$storage = $sys->storageDirectory().'/ngweather/';

	if(!file_exists($storage))
	{
		eZDir::mkdir($storage);
	}

	$ini_name = 'forecasts.ini';
	if(!file_exists($storage.$ini_name))
	{
		eZFile::create($ini_name, $storage);
		$new_ini = new eZINI($ini_name, $storage);
	}
	else
	{
		$new_ini = new eZINI($ini_name, $storage);
		$new_ini->reset();
	}

	foreach($forecast['result'] as $key => $result)
	{

		foreach($result as $condition_key => $condition_value)
		{
			if ($condition_key == '0')
			{
				foreach($condition_value as $primary_key => $primary_value)
				{
					$new_ini->setVariable($key, 'PrimaryArray['.$primary_key.']', $primary_value);
				}
			}
			else if ($condition_key == '1')
			{
				foreach($condition_value as $secondary_key => $secondary_value)
				{
					$new_ini->setVariable($key, 'SecondaryArray['.$secondary_key.']', $secondary_value);
				}
			}
		}
	}

	$new_ini->save();

	$cli->notice('All forecasts have been uploaded');
}else
{
	$cli->notice('Response error!');
}


?>
