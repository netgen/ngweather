<?php

class Weather
{

  function displayWeather( $name_of_ini )
  {
    $sys = eZSys::instance();
    $storage = $sys->storageDirectory().'/ngweather/';
    $forecast_ini = eZINI::instance($name_of_ini.'.ini', $storage);
    $data = array();
    foreach ($forecast_ini->getNamedArray() as $city => $result)
    {
      $primary = array();
      $secondary = array();
      foreach ($result as $result_key => $result_array)
      {
        if ($result_key == 'PrimaryArray')
        {
          $primary = $result_array;
        }
        elseif ($result_key == 'SecondaryArray')
        {
          $secondary = $result_array;
        }
      }
      $data[$city] = array($primary, $secondary);
    }

    return array( 'result' => $data );
  }

	public static function getForecast( $locationCode, $unit )
	{
	  ini_set('default_socket_timeout',    2);
	  $locations = array();
	  $data = array();

	  foreach ( $locationCode as $city => $code)
    {
        $locations[$city] = file_get_contents('http://rss.wunderground.com/auto/rss_full/global/stations/'.$code.'.xml?units='.$unit.'&lang=FR');
    }

    foreach ( $locations as $city => $location)
    {
      if (!$location)
      {
        $data[$city] = array(array('Désolé, mais le service n&#39;est pas disponible' => 'N/A'), array('img_link' => 'na'));
      }
      else
      {
        $dom = new DOMDocument( '1.0', 'utf-8' );
        $dom->preserveWhiteSpace = false;
        $dom->loadXML( $location );

        $condition_string = $dom->getElementsByTagName( 'item' )->item(0)->getElementsByTagName( 'description' )->item(0)->nodeValue;

        $condition_array = explode(' | ', $condition_string);
        $conditions_array = array();
        $non_display_cond = array();
        $counter = 0;
        foreach ($condition_array as $condition)
        {
          $counter++;
          $condition_exploded = explode(': ', $condition);
          if($counter == 1)
          {
            $non_display_cond['temp'] = $condition_exploded[1];
          }
          elseif($counter == 3)
          {
            $pressure = explode('h', $condition_exploded[1]);
            $value = explode('(', $condition_exploded[1]);
            $value_in_bracket = rtrim($value[1], ')');
            if (($pressure[0]>1000)&&($pressure[0]<1200)){
              $pressure_value = $pressure[0];
            }
            else
            {
              $pressure_value = '--';
            }
            $conditions_array[$condition_exploded[0]] = ($pressure_value.' hPa ('.$value_in_bracket.')');
          }
          elseif($counter == 4)
          {
            $non_display_cond['img_link'] = $condition_exploded[1];
          }
          elseif($counter==6)
          {
            $explode_condition_exploded = explode('<', $condition_exploded[1]);
            $conditions_array[$condition_exploded[0]] = $explode_condition_exploded[0];
          }
          else
          {
            $conditions_array[$condition_exploded[0]] = $condition_exploded[1];
          }
        }

        $data[$city] = array($conditions_array, $non_display_cond);
      }
    }
    return array( 'result' => $data );
  }

}

?>
