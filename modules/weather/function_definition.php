<?php

$FunctionList = array();

$FunctionList['forecast'] = array(
                'name' => 'forecast',
                'call_method' => array(
                            'include_file' => 'extension/ngweather/classes/class.Weather.php',
                            'class' => 'Weather',
                            'method' => 'displayWeather'
                          ),
                'parameter_type' => 'standard',
                'parameters' => array( array( 'name' => 'name_of_ini',
                                                             'type' => 'string',
                                                             'required' => true ))
              );

?>
